Goal:
 Determining the accumulated reward in a Markov Decision Process (namely the recycling robot).

Problem Definition:

A mobile robot has the job of collecting empty soda cans in an office environment.
It has sensors for detecting cans, and an arm and gripper that can pick them up
and place them in an onboard bin; it runs on a rechargeable battery. 
The robot's control system has components for interpreting sensory information,
for navigating, and for controlling the arm and gripper. 
High-level decisions about how to search for cans are made by a reinforcement learning
agent based on the current charge level of the battery.
This agent has to decide whether the robot should (1) actively search for a
can for a certain period of time, (2) remain stationary and wait for someone
to bring it a can, or (3) head back to its home base to recharge its battery.
This decision has to be made either periodically or whenever certain events
occur, such as finding an empty can. The agent therefore has three actions,
and its state is determined by the state of the battery. The rewards might be
zero most of the time, but then become positive when the robot secures an
empty can, or large and negative if the battery runs all the way down. In this
example, the reinforcement learning agent is not the entire robot. The states
it monitors describe conditions within the robot itself, not conditions of the
robot's external environment. The agent's environment therefore includes the
rest of the robot, which might contain other complex decision-making systems,
as well as the robot's external environment.