function [ action ] = RRagent( state,reward,rr )
%  m-function for agent of a recycling robot MDP 
%  with equiprobabale random policy for action selection 
%  [ action ] = RRagent( state,reward,rr ) 
global value
L=0;H=1;
if state==H   % High
    if rr<0.5
        action=1;   % search
    else
        action=2;   % wait
    end
elseif state==L %  Low
    if rr<1/3
        action=1;   % search
    elseif rr<2/3
        action=2;   % wait
    else
        action=3;   % recharge
    end
end
state=state+1;  % To change 0 and 1 to 1 and 2
% update value function 
value(state,action) = value(state,action)+reward;
end