% Main script for Recycling Robot Markov Decision Process (MDP); Mojtaba
% Nourani; 9523405064
%% Initializing
clear;close all;
global value
value=zeros(2,3);
myrand=rand(2,1000);
%% Training

st=0;rt=0;
for kk=1:1000
    at=RRagent(st,rt,myrand(1,kk));
    [stp,reward]=RRenvironment(at,st,myrand(2,kk));
    st=stp;rt=reward;
end
disp(value)