function [ FutureState,reward ] = RRenvironment( action,state,rr )
% m-function for envirinment of a recycling robot MDP
% [ FutureState,reward ] = RRenvironment( action,state,rr )
alpha=0.8;    % desision bound for high/low in search on state high
beta=0.2;     % desision bound for low/high in search on state low
R_search = 2 ; % expected no. of cans while searching
R_wait = 1;   % expected no. of cans while waiting
L=0;H=1;
if state==H   % High
    if action==1    % search
        reward = R_search;
        if rr<alpha
            FutureState = 1;     % High
        else
            FutureState = 0;     % Low
        end
    elseif action==2   % wait
        reward=R_wait;
        FutureState=1;   % High
    end
elseif state==L %  Low
    if action==1  % search
        if rr<beta
            reward = R_search;
            FutureState = 0;     % Low
        else
            reward = -3;
            FutureState = 1;     % High
        end
    elseif action==2  % wait
        reward=R_wait;
        FutureState=0;   % Low
    elseif action==3   % recharge
        reward=0;
        FutureState=1;   % High
    end
end